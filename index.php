<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>OOP PHP</title>
</head>
<body>
    <h1>Release 0</h1>
	<?php
		require_once('Animal.php');
		
		$sheep = new Animal("shaun");
		echo "Nama: ". $sheep->name. "<br>"; // "shaun"
		echo "Jumlah Kaki: ". $sheep->get_legs(). "<br>"; // 2
		echo "Berdarah Dingin: ". $sheep->get_cold_blooded(). "<br>" // false
	?>
	
	<h1>Release 1</h1>
	<?php
		require_once('Ape.php');
		require_once('Frog.php');
		
		$sungokong = new Ape("kera sakti");
		echo "Nama: ". $sungokong->name. "<br>";
		echo "Jumlah Kaki: ". $sungokong->get_legs(). "<br>";
		echo "Berdarah Dingin: ". $sungokong->get_cold_blooded(). "<br>";
		echo $sungokong->yell();
		echo "<br><br>";
		$kodok = new Frog("buduk");
		//$kodok->set_legs(4);
		echo "Nama: ". $kodok->name. "<br>";
		echo "Jumlah Kaki: ". $kodok->get_legs(). "<br>";
		echo "Berdarah Dingin: ". $kodok->get_cold_blooded(). "<br>";
		echo $kodok->jump();
	?>
    
</body>
</html>